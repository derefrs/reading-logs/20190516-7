# 20190516 - Deref.rs \#7

Thu, 16. May. 2019

https://derefrs.connpass.com/event/130365/

```toml
[meetup]
name = "Deref Rust"
date = "20190516"
version = "0.0.7"
attendees = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20190516-7"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @grauwoelfchen | Read Chapter 4 "Lists, Lists, and More Lists" of a book [Hands-On Data Structures and Algorithms with Rust](https://www.packtpub.com/application-development/hands-data-structures-and-algorithms-rust) (978-1-78899-552-8)|


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2019 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
